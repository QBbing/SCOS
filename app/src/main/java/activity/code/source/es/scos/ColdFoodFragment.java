package activity.code.source.es.scos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import es.source.code.model.Food;


/**
 * A simple {@link Fragment} subclass.
 */
public class ColdFoodFragment extends BaseFoodFragment {

    @Override
    public void initFoods(){
        foodlist.clear();
        for (int i = 0;i < 2;i++){
            Food coldFood = new Food(R.drawable.ic_colddish_1,"10","腐竹");
            foodlist.add(coldFood);
            Food coldFood_2 = new Food(R.drawable.ic_colddish_2,"15","辣椒皮蛋");
            foodlist.add(coldFood_2);
            Food coldFood_3 = new Food(R.drawable.ic_colddish_3,"10","土豆丝");
            foodlist.add(coldFood_3);
        }
    }
}
