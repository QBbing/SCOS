package activity.code.source.es.scos;

import android.app.MediaRouteButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Help_view extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_view);
        TextView view = (TextView) findViewById(R.id.txt2);
        view.setMovementMethod(ScrollingMovementMethod.getInstance());
    }
}

