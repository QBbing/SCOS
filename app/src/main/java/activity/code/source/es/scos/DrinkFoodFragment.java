package activity.code.source.es.scos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.source.code.model.Food;


/**
 * A simple {@link Fragment} subclass.
 */
public class DrinkFoodFragment extends BaseFoodFragment {

    @Override
    public void initFoods() {
        foodlist.clear();
        for (int i = 0;i < 2;i++){
            Food drinking_1 = new Food(R.drawable.ic_drinking_1,"6","魔爪");
            foodlist.add(drinking_1);
            Food drinking_2 = new Food(R.drawable.ic_drinking_2,"6","红牛");
            foodlist.add(drinking_2);
            Food drinking_3 = new Food(R.drawable.ic_drinking_3,"6","战马");
            foodlist.add(drinking_3);
            Food drinking_4 = new Food(R.drawable.ic_drinking_4,"3","百事可乐");
            foodlist.add(drinking_4);
        }
    }
}
