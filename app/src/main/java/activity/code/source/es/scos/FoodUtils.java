package activity.code.source.es.scos;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import es.source.code.model.Food;

import static android.content.ContentValues.TAG;

public class FoodUtils extends AppCompatActivity {

    private Context mContext;
    private static List<Food> foodList = new ArrayList<>();

    public List<Food> getFoodList(){
        return foodList;
    }

    public FoodUtils() {
        super();
        this.mContext = SCOSEntry.getAppContext();
    }


    public void addNotOrderFood(Food e){
        Log.d(TAG, "addNotOrderFood: " + e.getName());
        foodList.add(e);
        for (Food i:
                foodList) {
            Log.d(TAG, "FoodList: " + i.getName());
        }
    }

    public void removeNotOrderFood(Food e){
        Log.d(TAG, "removeNotOrderFood: " + e.getName());
        Iterator<Food> iter = foodList.iterator();
        while (iter.hasNext()) {
            Food item = iter.next();
            if (item.equals(e)) {
                iter.remove();
            }
        }

        for (Food i:
                foodList) {
            Log.d(TAG, "FoodList: " + i.getName());
        }
    }
    public void clearNotOrderFood(){
        save("foodList.txt","");
    }

    public void saveNotOrderFood(){
        String fileContent = "";
        for (Food i:
             foodList) {
            fileContent += i.getImageView() + "sp" + i.getPrice() + "sp" + i.getName();
            Log.d(TAG, "saveNotOrderFood: " + i.getName());
            fileContent += "-";
        }
        fileContent= fileContent.substring(0,fileContent.length()-1);
        save("notOrderFoodList.data",fileContent);
    }

    public void readNotOrderFood(){
        String fileContent = "0sp0sp0";
        try {
            fileContent = read("notOrderFoodList.data");

            List<Food> readFoodList = new ArrayList<>();
            String[] foodString = fileContent.split("-");
            for (String i:
                    foodString) {
                String[] tmp = i.split("sp");
                Food singalFood = new Food(Integer.parseInt(tmp[0]), tmp[1], tmp[2]);
                Log.d(TAG, "readNotOrderFood: " + singalFood.getName());
                readFoodList.add(singalFood);
            }
            foodList = readFoodList;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
     * 这里定义的是一个文件保存的方法，写入到文件中，所以是输出流
     * */
    private void save(String filename, String filecontent) {
        //这里我们使用私有模式,创建出来的文件只能被本应用访问,还会覆盖原文件哦
        try{
            FileOutputStream output = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            output.write(filecontent.getBytes());  //将String字符串以字节流的形式写入到输出流中
            output.close();         //关闭输出流
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /*
     * 这里定义的是文件读取的方法
     * */
    private String read(String filename) throws IOException {
            //打开文件输入流
            FileInputStream input = mContext.openFileInput(filename);
            byte[] temp = new byte[1024];
            StringBuilder sb = new StringBuilder("");
            int len = 0;
            //读取文件内容:
            while ((len = input.read(temp)) > 0) {
                sb.append(new String(temp, 0, len));
            }
            //关闭输入流
            input.close();
            return sb.toString();
    }

}
